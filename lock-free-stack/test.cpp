#include <gtest/gtest.h>
#include <util.h>

#include <chrono>
#include <thread>
#include <vector>
#include <string>
#include <atomic>
#include <iostream>

#include <stack.h>

using namespace std::chrono;  // NOLINT

TEST(Correctness, Order) {
    Stack<int> s;
    s.Push(1);
    s.Push(2);
    s.Push(3);
    int value;
    ASSERT_TRUE(s.Pop(&value));
    ASSERT_EQ(3, value);
    ASSERT_TRUE(s.Pop(&value));
    ASSERT_EQ(2, value);
    ASSERT_TRUE(s.Pop(&value));
    ASSERT_EQ(1, value);
    ASSERT_FALSE(s.Pop(&value));
    ASSERT_EQ(0u, s.Size());
}

TEST(Correctness, Pushes) {
    const int count = 10000;
    Stack<int> s;
    std::thread t1([&s, count]() {
        for (int i = 0; i < count; i += 2) {
            s.Push(i);
        }
    });
    std::thread t2([&s, count]() {
        for (int i = 1; i < count; i += 2) {
            s.Push(i);
        }
    });
    t1.join();
    t2.join();

    ASSERT_EQ(count, static_cast<int>(s.Size()));
    std::vector<int> used(count);
    int value;
    int popped = 0;
    std::vector<int> last(2);
    last[0] = count;
    last[1] = count + 1;
    while (s.Pop(&value)) {
        ASSERT_LE(0, value);
        ASSERT_LT(value, count);
        ASSERT_FALSE(used[value]);
        used[value] = true;
        ++popped;
        ASSERT_EQ(last[value % 2] - 2, value);
        last[value % 2] = value;
    }
    ASSERT_EQ(count, popped);
}

TEST(Correctness, Pops) {
    const int count = 100;
    Stack<int> s;
    for (int i = 0; i < count; ++i) {
        s.Push(i);
    }

    ASSERT_EQ(count, static_cast<int>(s.Size()));
    std::vector<std::atomic<int>> used(count);

    auto func = [&s, count, &used]() {
        int value;
        while (s.Pop(&value)) {
            ASSERT_LE(0, value);
            ASSERT_LT(value, count);
            ++used[value];
        }
    };

    std::thread t1(func);
    std::thread t2(func);
    t1.join();
    t2.join();

    for (int i = 0; i < count; ++i) {
        ASSERT_EQ(1, used[i].load());
    }
}

std::vector<std::string> Merge(const std::vector<std::vector<std::string>>& data) {
    std::vector<std::string> result;
    for (const auto& row : data) {
        result.insert(result.end(), row.begin(), row.end());
    }
    return result;
}

TEST(Correctness, PushPop) {
    const int iterations = 4000;
    const int push_threads = 6;
    const int pop_threads = 2;
    std::vector<std::vector<std::string>> pushed(push_threads);
    std::vector<std::thread> threads;
    Stack<std::string> stack;
    std::atomic<int> push_finished(0);
    for (int i = 0; i < push_threads; ++i) {
        threads.emplace_back([&stack, iterations, &pushed, i, &push_finished]() {
            RandomGenerator gen(3675475 * (i + 1));
            for (int j = 0; j < iterations; ++j) {
                auto str = gen.GenString(8);
                pushed[i].push_back(str);
                stack.Push(str);
            }
            ++push_finished;
        });
    }

    std::vector<std::vector<std::string>> popped(pop_threads);
    for (int i = 0; i < pop_threads; ++i) {
        threads.emplace_back([&stack, &popped, i, &push_finished, push_threads]() {
            std::string value;
            for (;;) {
                bool pop = stack.Pop(&value);
                if (!pop && push_finished.load() == push_threads) {
                    break;
                }
                if (!pop) {
                    continue;
                }
                popped[i].push_back(value);
            }
        });
    }

    for (auto& cur : threads) {
        cur.join();
    }

    auto all_pushed = Merge(pushed);
    auto all_popped = Merge(popped);
    std::sort(all_pushed.begin(), all_pushed.end());
    std::sort(all_popped.begin(), all_popped.end());

    ASSERT_EQ(all_pushed, all_popped);
}
