# Программа второго семестра

1. Потоки. Старт потока, join(). Мютексы. Guard-ы, defer lock. Определение data race.
   Атомарные переменные. RW-Lock. В каких ситуациях можно использовать detach().
   Работа с google benchmark. CPU time vs Wall Time. Отладка дедлоков, отладка TSAN.

2. Условная синхронизация. Правильное использование Condition Variable.
   Разбор домашнего задания про jpeg декодер.

3. Модель памяти и атомарные переменные. Барьеры. Hazard Pointers, реализация и примеры
   использования (atomic_shared_ptr, sync_map, stack).

4. Реализация примитивов синхронизации на linux. futex. Примитив event count. MPSC Stack.
   RSEQ и per-cpu структуры данных.

5. Специализация шаблонов. Вычисление функций с помощью специализации. SFINAE, заголовок функции.
   Probing свойств типа, используя sfinae. Шаблоны с переменным числом аргументов. Мета-функции, <type_traits>.
   Разбор домашнего задания про thread pool. Реализация таймеров, отмен. Управление памятью. Решение проблемы
   дедлоков.

6. constexpr, constinit, consteval вырашения. constexpr if как альтернатива SFINAE. Концепты.

7. Network #1

8. Network #2

9. GDB. Основные команды, печать переменных, стека, тредов. Отладка coredump. Санитайзеры.
    Алгоритм работы asan/msan. Детектируемые виды ошибок. Аннотации asan и msan.
    Алгоритм работы tsan. Виды ошибок. Аннотации на блокировки.

10. Network #3

11. Рассказ про общую архитектуру, preprocessor-front-middle-back. Рассказ про clang-AST. AST-matchers
    Разбор примера на clang-AST, подробный разбор AST матчера.

12. Введение в IR. Показ кодогенерации из Front в IR, мб показ phi функций максимум.
    (может быть широкие регистры в SIMD). Уровни оптимизации. Тюнинг опций в зависимости от железа.
    Рассказ про llvm pass manager, старый и новый, потом llvm passes. Разбор какого-нибудь llvm pass,
    например, FoldConstant на какой-нибудь арифметике.

13. Производительность. Всё что нужно знать про память. Микроархитектура процессора.
    Как пользоваться perf.
