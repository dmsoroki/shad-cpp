#include <benchmark/benchmark.hpp>
#include <vector>
#include <random>

class Matrix {
public:
    explicit Matrix(size_t size): data_(size * size) {
        size_ = size;
    }

    int *operator[](size_t i) {
        return &data_[i * size_];
    }

    const int *operator[](size_t i) const {
        return &data_[i * size_];
    }

    size_t size() const {
        return size_;
    }
private:
    std::vector<int> data_;
    size_t size_;
};

Matrix gen_matrix(int size, int seed) {
    Matrix a(size);
    std::mt19937 gen(seed);
    std::uniform_int_distribution<int> dist(0, 100);
    for (int i = 0; i < size; ++i)
        for (int j = 0; j < size; ++j)
            a[i][j] = dist(gen);
    return a;
}

Matrix multiply(const Matrix& a, const Matrix& b) {
    size_t n = a.size();
    Matrix c(n);
    for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < n; ++j) {
            int sum = 0;
            for (size_t k = 0; k < n; ++k)
                sum += a[i][k] * b[k][j];
            c[i][j] = sum;
        }
    return c;
}

Matrix multiply_better(const Matrix& a, Matrix b) {
    size_t n = a.size();
    Matrix c(n);
    for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < i; ++j)
            std::swap(b[i][j], b[j][i]);
    for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < n; ++j) {
            int sum = 0;
            for (size_t k = 0; k < n; ++k)
                sum += a[i][k] * b[j][k];
            c[i][j] = sum;
        }
    return c;
}

const int kSeed = 8387457;

template<int type>
void run(benchmark::State& state) {
    int size = state.range(0);
    Matrix a = gen_matrix(size, kSeed);
    Matrix b = gen_matrix(size, kSeed + type * 100);
    while (state.KeepRunning()) {
        if (type == 0)
            benchmark::DoNotOptimize(multiply(a, b));
        else
            benchmark::DoNotOptimize(multiply_better(a, b));
    }
}

BENCHMARK_TEMPLATE(run, 0)->Arg(2048)->Arg(2049)->Unit(benchmark::kMillisecond);
BENCHMARK_TEMPLATE(run, 1)->Arg(2048)->Arg(2049)->Unit(benchmark::kMillisecond);

BENCHMARK_MAIN();
